# ODM and MongoDB

## Creating this markdown
As bitbucket does not seem to have a preview I followed the forum's advice and used [Hashify](http://hashify.me)

## Creating this project

 1. Create project using ``npm init`` and accepting defaults
 2. You should now have package.json
 3. Inspect package.json and add ``"private":true`` to prevent publish back to npm.
 4. install mongoose via ``npm install mongoose --save`` which adds it to the package.json
 5. Check mongoose dependency added to package.json
 
## App.js
``var routes = require('./routes')`` means look for ``/routes/index.js`` in the current directory

## Jade templates
Inspecting ``layout.jade`` we can see the definition of block content:

    section.content
        block content
        
Now ``home.jade`` replaces this section by

    extends layout
    ...
    block content
       if !(posts && posts.length)
       ...
       
Also note that the html tags 'include' the '=' so

    h1= pageTitle
    
is correct but

    h1 = pageTitle
   
is not.

## Express error handling
From ``/routes/errors.js`` we can see that there are two functions, one handling 404s and another actual errors. We know the difference by the method signatures:

    app.use(function(req, res, next) {...}
    
and

    app.use(function(err, req, res, next) {...}
   